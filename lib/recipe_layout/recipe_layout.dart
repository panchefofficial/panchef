import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:recipient/model/RecipeModel.dart';
import 'package:recipient/firebase_request.dart';
import 'package:recipient/model/register_model.dart';
import 'package:recipient/util.dart';

import '../detail/detail_screen.dart';

class RecipeLayout extends StatefulWidget {
  const RecipeLayout({Key? key, required this.recipe, required this.userUid})
      : super(key: key);

  final RecipeModel recipe;
  final String userUid;

  @override
  State<RecipeLayout> createState() => _RecipeLayoutState();
}

class _RecipeLayoutState extends State<RecipeLayout> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailScreen(recipe: widget.recipe),
          ),
        );
      },
      child: SizedBox(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 190,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: CachedNetworkImage(
                        imageUrl: widget.recipe.photoUrl,
                        placeholder: (context, _) => const Image(
                          image: AssetImage("asset/image/placeholder_food.jpg"),
                        ),
                        errorWidget: (context, _, __) => const Image(
                          image: AssetImage("asset/image/placeholder_food.jpg"),
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Container(
                    width: 100,
                    margin: const EdgeInsets.only(top: 8.0, left: 8.0),
                    child: Text(
                      widget.recipe.title,
                      textAlign: TextAlign.start,
                      maxLines: 2,
                      style: const TextStyle(
                        fontFamily: mainFont,
                        fontWeight: FontWeight.w700,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  FutureBuilder(
                      future: averageRating(widget.recipe.recipeId.toString()),
                      builder: (context, snapshot) {
                        if (snapshot.data?.isNotEmpty == true) {

                          int r = snapshot.data?.values.reduce((value, element) => value + element);
                          double rating = 0;

                          for (var element in snapshot.data!.entries) {
                            rating += double.parse(element.key.toString()) * double.parse(element.value.toString());
                          }

                          return Padding(
                            padding: const EdgeInsets.only(left: 8, top: 8),
                            child: RatingBar(
                              itemSize: 10,
                              initialRating: (rating / r).floorToDouble(),
                              ignoreGestures: true,
                              ratingWidget: RatingWidget(
                                full: const Icon(
                                  Icons.star,
                                  color: Colors.yellow,
                                ),
                                half: const Icon(
                                  Icons.star_half,
                                  color: Colors.yellow,
                                ),
                                empty: const Icon(
                                  Icons.star,
                                  color: Colors.black26,
                                ),
                              ),
                              onRatingUpdate: (value) {},
                            ),
                          );
                        } else {
                          return Padding(
                            padding: const EdgeInsets.only(left: 8, top: 8),
                            child: RatingBar(
                              itemSize: 10,
                              initialRating: 0,
                              ignoreGestures: true,
                              ratingWidget: RatingWidget(
                                full: const Icon(
                                  Icons.star,
                                  color: Colors.yellow,
                                ),
                                half: const Icon(
                                  Icons.star_half,
                                  color: Colors.yellow,
                                ),
                                empty: const Icon(
                                  Icons.star,
                                  color: Colors.black26,
                                ),
                              ),
                              onRatingUpdate: (value) {},
                            ),
                          );
                        }
                      }
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0, bottom: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            const Icon(
                              Icons.person,
                              size: 12,
                              color: Colors.black26,
                            ),
                            Text(
                              "${widget.recipe.portion}",
                              style: const TextStyle(
                                  fontFamily: mainFont,
                                  color: Colors.black26,
                                  fontSize: 9),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const Icon(
                              Icons.timer_outlined,
                              size: 12,
                              color: Colors.black26,
                            ),
                            Text(
                              "${widget.recipe.duration} menit",
                              style: const TextStyle(
                                  fontFamily: mainFont,
                                  color: Colors.black26,
                                  fontSize: 9),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection("User Information")
                      .doc(widget.userUid)
                      .collection("Favorite")
                      .doc(widget.recipe.recipeId.toString())
                      .snapshots(),
                  builder: (context, snapshot) {
                    return Positioned(
                      top: 10,
                      right: 10,
                      child: GestureDetector(
                        onTap: () async {
                          if (snapshot.data?.exists == true) {
                            await deleteFavorite(widget.recipe, widget.userUid);
                          } else {
                            await addFavorite(widget.recipe, widget.userUid);
                          }
                        },
                        child: Card(
                          shape: const CircleBorder(),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Icon(
                              snapshot.data?.exists == true
                                  ? Icons.favorite
                                  : Icons.favorite_border,
                              size: 20,
                              color: snapshot.data?.exists == true
                                  ? Colors.red
                                  : Colors.black54,
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
              Positioned(
                right: 15,
                top: 175,
                child: FutureBuilder<RegisterModel>(
                  future: getProfile(widget.recipe.uploaderId),
                  builder: (context, snapshot) {
                    return Card(
                        shape: const CircleBorder(),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15.0),
                            child: CachedNetworkImage(
                              height: 20,
                              width: 20,
                              fit: BoxFit.cover,
                              imageUrl: snapshot.data?.photoUrl ?? '',
                              placeholder: (context, _) => const Icon(
                                Icons.account_circle_rounded,
                                color: Colors.black54,
                                size: 20,
                              ),
                              errorWidget: (context, _, __) => const Icon(
                                Icons.account_circle_rounded,
                                color: Colors.black54,
                                size: 20,
                              ),
                            ),
                          ),
                        ));
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
