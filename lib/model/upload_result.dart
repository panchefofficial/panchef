class UploadResult {
  String? url;
  String? error;

  UploadResult(this.url, this.error);
}