import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:recipient/model/RecipeModel.dart';
import 'package:recipient/firebase_request.dart';
import 'package:recipient/login/login_screen.dart';
import 'package:recipient/recipe_layout/my_recipe_layout.dart';
import 'package:recipient/util.dart';

import 'Provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String userUid = "";

  bool isLoading = false;

  @override
  void initState() {
    userUid = FirebaseAuth.instance.currentUser?.uid ?? "";
    Provider.of<MainProvider>(context, listen: false).fetchProfile(userUid);
    Provider.of<MainProvider>(context, listen: false).fetchUserRecipes(userUid);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Image.asset(
                        "asset/image/profile_bg.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        color: const Color.fromRGBO(230, 230, 230, 0.5),
                      ),
                    )
                  ],
                ),
                Positioned(
                  top: 200,
                  left: 5,
                  right: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          child: Container(
                            height: 120,
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "${Provider.of<MainProvider>(context).registerModel.firstName} ${Provider.of<MainProvider>(context).registerModel.lastName}",
                                  style: const TextStyle(
                                      fontFamily: mainFont, fontSize: 18),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    StreamBuilder(
                                        stream: FirebaseFirestore.instance
                                            .collection("User Information")
                                            .doc(userUid)
                                            .collection("Followers")
                                            .snapshots(),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            return Text(
                                              "${snapshot.data?.docs.length} Pengikut",
                                              style: const TextStyle(
                                                  fontFamily: mainFont,
                                                  color: Colors.black54),
                                            );
                                          } else {
                                            return const Text(
                                              "0 Pengikut",
                                              style: TextStyle(
                                                  fontFamily: mainFont,
                                                  color: Colors.black54),
                                            );
                                          }
                                        }),
                                    const Text(
                                      ".",
                                      style: TextStyle(
                                          fontFamily: mainFont, fontSize: 20),
                                    ),
                                    StreamBuilder(
                                      stream: FirebaseFirestore.instance
                                          .collection("User Information")
                                          .doc(userUid)
                                          .collection("Followings")
                                          .snapshots(),
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) {
                                          return Text(
                                            "${snapshot.data?.docs.length} Diikuti",
                                            style: const TextStyle(
                                                fontFamily: mainFont,
                                                color: Colors.black54),
                                          );
                                        } else {
                                          return const Text(
                                            "0 Diikuti",
                                            style: TextStyle(
                                                fontFamily: mainFont,
                                                color: Colors.black54),
                                          );
                                        }
                                      },
                                    ),
                                  ],
                                )
                              ],
                            ),
                          )),
                      const Padding(
                        padding: EdgeInsets.only(left: 8.0, top: 24.0),
                        child: Text(
                          "Resep Anda",
                          style: TextStyle(
                            fontFamily: mainFont,
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      StreamBuilder(
                          stream: FirebaseFirestore.instance
                              .collection("Recipe")
                              .where('status', isEqualTo: 'publish')
                              .where('uploaderId', isEqualTo: userUid)
                              .snapshots(),
                          builder: (context, snapshot) {
                            if (snapshot.data?.docs.isNotEmpty == true) {
                              return StaggeredGrid.count(
                                crossAxisCount: 2,
                                children: snapshot.data!.docs.map((e) {
                                  var recipe = RecipeModel.fromMap(e.data());
                                  return MyRecipeLayout(
                                    recipe: recipe,
                                    profileUrl:
                                        Provider.of<MainProvider>(context)
                                            .registerModel
                                            .photoUrl,
                                    mode: 'my',
                                  );
                                }).toList(),
                              );
                            } else {
                              return Expanded(
                                child: Center(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Image.asset(
                                        'asset/image/empty_img.png',
                                        height: 100,
                                        width: 100,
                                      ),
                                      const Text(
                                        "Belum ada resep",
                                        style:
                                        TextStyle(fontFamily: mainFont, fontSize: 16),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }
                          }),
                    ],
                  ),
                ),
                Positioned(
                  top: 140,
                  left: 1,
                  right: 1,
                  child: GestureDetector(
                    onTap: () async {
                      ImagePicker picker = ImagePicker();
                      final XFile? file =
                          await picker.pickImage(source: ImageSource.gallery);
                      if (file != null) {
                        setState(() {
                          isLoading = true;
                        });
                        uploadProfileImage(File(file.path), userUid).then(
                          (value) {
                            if (value.error == null &&
                                value.url?.isNotEmpty == true) {
                              updateProfile(value.url!, userUid)
                                  .whenComplete(() {
                                setState(() {
                                  isLoading = false;
                                });
                              });
                            } else {
                              Fluttertoast.showToast(
                                msg: "Unggahan Berhasil",
                                toastLength: Toast.LENGTH_SHORT,
                              );
                              setState(() {
                                isLoading = false;
                              });
                            }
                          },
                        );
                      }
                    },
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 60,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(80),
                        child: CachedNetworkImage(
                          height: 120,
                          width: 120,
                          imageUrl: Provider.of<MainProvider>(context)
                              .registerModel
                              .photoUrl,
                          placeholder: (context, _) => const Icon(
                            Icons.account_circle_rounded,
                            color: Colors.black54,
                            size: 120,
                          ),
                          errorWidget: (context, _, __) => const Icon(
                            Icons.account_circle_rounded,
                            size: 120,
                            color: Colors.black54,
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 160,
                  left: 1,
                  right: 1,
                  child: Visibility(
                    visible: isLoading,
                    child: Lottie.asset("asset/animation/data.json",
                        width: 100, height: 100, animate: isLoading),
                  ),
                ),
                Positioned(
                    top: 50,
                    right: 1,
                    child: GestureDetector(
                      onTap: () {
                        showDialog(
                          barrierDismissible: false,
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                content: const Text(
                                  "Apakah anda yakin ingin keluar?",
                                  style: TextStyle(fontFamily: mainFont),
                                ),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      FirebaseAuth.instance.signOut();
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => const LoginScreen(),
                                        ),
                                      );
                                    },
                                    style: const ButtonStyle(
                                      overlayColor: MaterialStatePropertyAll(Color.fromRGBO(255, 240, 230, 1),)
                                    ),
                                    child: const Text(
                                      "Ya",
                                      style: TextStyle(fontFamily: mainFont, color: mainColor),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    style: const ButtonStyle(
                                        overlayColor: MaterialStatePropertyAll(Color.fromRGBO(255, 240, 230, 1),)
                                    ),
                                    child: const Text(
                                      "Tidak",
                                      style: TextStyle(fontFamily: mainFont, color: mainColor),
                                    ),
                                  )
                                ],
                              );
                            });
                      },
                      child: const Card(
                        shape: CircleBorder(),
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(Icons.logout),
                        ),
                      ),
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
